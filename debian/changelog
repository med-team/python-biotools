python-biotools (1.2.12-6) unstable; urgency=medium

  * Team upload.
  * Add dependency on python3-setuptools (Closes: #1080909)
  * Switch to DebHelper 13
  * Set +Rules-Requires-Root: no

 -- Alexandre Detiste <tchet@debian.org>  Sat, 21 Sep 2024 16:25:04 +0200

python-biotools (1.2.12-5) unstable; urgency=medium

  * Really drop Python2 support
    Closes: #937607
  * Standards-Version: 4.4.1
  * Set upstream metadata fields: Bug-Database.

 -- Andreas Tille <tille@debian.org>  Fri, 13 Dec 2019 12:13:05 +0100

python-biotools (1.2.12-4) unstable; urgency=medium

  * Drop Python2 support
    Closes: #937607
  * debhelper-compat 12
  * Standards-Version: 4.4.0

 -- Andreas Tille <tille@debian.org>  Wed, 04 Sep 2019 14:44:40 +0200

python-biotools (1.2.12-3) unstable; urgency=medium

  * Testsuite: autopkgtest-pkg-python
  * Standards-Version: 4.1.4
  * Point Vcs fields to salsa.debian.org
  * debhelper 11
  * d/watch: secure URI

 -- Andreas Tille <tille@debian.org>  Mon, 16 Apr 2018 11:01:38 +0200

python-biotools (1.2.12-2) unstable; urgency=medium

  [ Scott Kitterman ]
  * Correct arch:any -> arch:all since package has no compiled content
    Closes: #866528

  [ Andreas Tille ]
  * Standards-Version: 4.1.0 (no changes needed)

 -- Andreas Tille <tille@debian.org>  Thu, 31 Aug 2017 16:05:02 +0200

python-biotools (1.2.12-1) unstable; urgency=medium

  * Initial release (Closes: #856597)

 -- Andreas Tille <tille@debian.org>  Thu, 02 Mar 2017 21:14:04 +0100
